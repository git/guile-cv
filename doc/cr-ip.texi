@c -*- mode: texinfo; coding: utf-8 -*-
@c This is part of the GNU Guile-CV Reference Manual.
@c Copyright (C) 2016 - 2023 Free Software Foundation, Inc.
@c See the file guile-cv.texi for copying conditions.


@menu
* Image Structure and Accessors::
* Kernel Structure and Accessors::
* Import Export::
* Histogram::
* Texture::
* Features::
* Particles::
* Process::
* Filter::
* Transform::
* Morphology::
* Segmentation::
* Compose::
* Utilities::
@end menu


@include cr-ip-idata.texi
@include cr-ip-kdata.texi
@include cr-ip-impex.texi
@include cr-ip-histogram.texi
@include cr-ip-texture.texi
@include cr-ip-features.texi
@include cr-ip-particles.texi
@include cr-ip-filter.texi
@include cr-ip-process.texi
@include cr-ip-transform.texi
@include cr-ip-morphology.texi
@include cr-ip-segmentation.texi
@include cr-ip-compose.texi
@include cr-ip-utils.texi
