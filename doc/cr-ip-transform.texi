@c -*- mode: texinfo; coding: utf-8 -*-
@c This is part of the GNU Guile-CV Reference Manual.
@c Copyright (C) 2016 - 2023 Free Software Foundation, Inc.
@c See the file guile-cv.texi for copying conditions.


@node Transform
@subsection Transform

The Guile-CV procedures and methods to transform images.

@subheading Procedures

@ifhtml
@indentedblock
@table @code
@item @ref{im-rgba->rgb}
@item @ref{im-rgba->gray}
@item @ref{im-rgb->gray}
@item @ref{im-resize}
@item @ref{im-resize-channel}
@item @ref{im-rotate}
@item @ref{im-rotate-channel}
@item @ref{im-flip}
@item @ref{im-flip-channel}
@item @ref{im-invert}
@item @ref{im-invert-channel}
@item @ref{im-transpose}
@item @ref{im-transpose-channel}
@item @ref{im-complement}
@item @ref{im-clip}
@item @ref{im-clip-channel}
@item @ref{im-crop}
@item @ref{im-crop-channel}
@item @ref{im-crop-size}
@item @ref{im-padd}
@item @ref{im-padd-channel}
@item @ref{im-padd-size}
@item @ref{im-local-minima}
@item @ref{im-local-minima-channel}
@item @ref{im-local-maxima}
@item @ref{im-local-maxima-channel}
@item @ref{im-fft}
@item @ref{im-fft-channel}
@item @ref{im-fft-inverse}
@item @ref{im-fft-inverse-channel}
@item @ref{im-fcc}
@item @ref{im-fcc-channel}
@item @ref{im-fncc}
@item @ref{im-fncc-channel}
@end table
@end indentedblock
@end ifhtml


@anchor{im-rgba->rgb}
@anchor{im-rgba->gray}
@anchor{im-rgb->gray}
@deffn Procedure im-rgba->rgb image [#:bg '(0.0 0.0 0.0)]
@deffnx Procedure im-rgba->gray image [#:bg '(0.0 0.0 0.0)]
@deffnx Procedure im-rgb->gray image

Returns a new RGB or GRAY image.

In the RGBA case, @var{image} channels are first normalized. The new RGB
channels are obtained by applying the following pseudo code algorithm:

@lisp
R = (((1 - Source.A) * BG.R) + (Source.A * Source.R)) * 255.0
G = (((1 - Source.A) * BG.G) + (Source.A * Source.G)) * 255.0
B = (((1 - Source.A) * BG.B) + (Source.A * Source.B)) * 255.0
@end lisp
@end deffn


@anchor{im-resize}
@anchor{im-resize-channel}
@deffn Procedure im-resize image new-width new-height @
       [#:i-mode 'bilinear]
@deffnx Procedure im-resize-channel channel width height new-width new-height @
        [#:i-mode 'bilinear]

Returns a new image or chanbnel resized to @var{new-width},
@var{new-height}.

The interpolation mode @var{#:i-mode}, can be one of:

@indentedblock
@table @code
@item none
@item bilinear
@item biquadratic
@item bicubic
@item ? (fixme) 
@end table
@end indentedblock
@end deffn


@anchor{im-rotate}
@anchor{im-rotate-channel}
@deffn Procedure im-rotate image angle [#:i-mode 'bilinear]
@deffnx Procedure im-rotate-channel channel width height angle @
        [#:i-mode 'bilinear]

Returns a new image or channel rotated by @var{angle}.

The @var{angle} is in degrees: @code{+/-[0.0 360.0]}.

It is neccessary, for rotations other than multiples of 90°, to
recalculate the target coordinates, since after the rotation, they might
be floats.  The 'next neighbor' interpolation possible modes,
@var{#:i-mode}, are:

@indentedblock
@table @code
@item bilinear
@item biquadratic
@item bicubic
@item ? (fixme)
@end table
@end indentedblock
@end deffn


@anchor{im-flip}
@anchor{im-flip-channel}
@deffn Procedure im-flip image plane
@deffnx Procedure im-flip-channel channel width height plane

Returns a new image or channel flipped according to the selected
@var{plane}.

Valid flipping @var{plane} values are:

@indentedblock
@table @code
@item hori horizontal
@item vert vertical
@item both
@end table
@end indentedblock
@end deffn


@anchor{im-invert}
@anchor{im-invert-channel}
@deffn Procedure im-invert image
@deffnx Procedure im-invert-channel channel width height

Returns a new inversed image or channel.

Calculating the inverse of an @var{image} or a @var{channel} consist of
applying the exponent function, @code{expt}, to all pixel values,
raising them to the power of -1.
@end deffn


@anchor{im-transpose}
@anchor{im-transpose-channel}
@deffn Procedure im-transpose image
@deffnx Procedure im-transpose-channel channel width height

Returns a new tranposed image or channel.

Transposing an @var{image} or a @var{channel} consist of flipping it
over its main diagonal.  In the transposed result, switched in size, row
values are the original column values and column values are the original
row values.
@end deffn


@anchor{im-complement}
@deffn Procedure im-complement image

Returns a new image.

This procedure computes the mathematical complement of @var{image},
which for Guile-CV means that for each pixel of each channel, the new
value is @code{(- 255.0 pixel-value)}.
@end deffn


@anchor{im-clip}
@anchor{im-clip-channel}
@deffn Procedure im-clip image [#:lower 0.0] [#:upper 255.0]
@deffnx Procedure im-clip-channel channel width height [#:lower 0.0] [#:upper 255.0]

Returns a new clipped image or channel.

Clipping an @var{image} or a @var{channel} consist of replacing all
pixel values below @code{lower} by the @code{lower} value and all pixel
values above @code{upper} by the @code{upper} value.
@end deffn


@anchor{im-crop}
@anchor{im-crop-channel}
@deffn Procedure im-crop image left top right bottom
@deffnx Procedure im-crop-channel channel width height left top right bottom @
        [#:new-w #f] [#:new-h #f]

Returns a new image, resulting of the crop of @var{image} at @var{left},
@var{top}, @var{right} and @var{bottom}.
@end deffn


@anchor{im-crop-size}
@deffn Procedure im-crop-size width height left top right bottom

Returns a list, @code{(new-width new-height)}.

Given the original image @var{width} and @var{height}, this procedure
checks that @var{left}, @var{top}, @var{right} and @var{bottom} are
valid and return a list, @code{(new-width new-height)}, otherwise, it
raises an error.
@end deffn


@anchor{im-padd}
@anchor{im-padd-channel}
@deffn Procedure im-padd image left top right bottom [#:color '(0.0 0.0 0.0)]
@deffnx Procedure im-padd-channel channel width height left top right bottom @
        [#:new-w #f] [#:new-h #f] [#:value 0.0]

Returns a new image or channel, respectively padding @var{image} or
@var{channel} by @var{left}, @var{top}, @var{right} and @var{bottom}
pixels initialized respectively to @var{color} or @var{value}. Note
that when @code{im-padd} is called upon a @code{GRAY} image,
@var{color} is reduced to its corresponding gray @var{value}:

@lisp
@code{(/ (reduce + 0 color) 3)}
@end lisp
@end deffn


@anchor{im-padd-size}
@deffn Procedure im-padd-size width height left top right bottom

Returns a list, @code{(new-width new-height)}.

Given the original image @var{width} and @var{height}, this procedure
checks that @var{left}, @var{top}, @var{right} and @var{bottom} are
@code{>= 0} and return a list, @code{(new-width new-height)},
otherwise, it raises an error.
@end deffn


@anchor{im-local-minima}
@anchor{im-local-maxima}
@anchor{im-local-minima-channel}
@anchor{im-local-maxima-channel}
@deffn Procedure im-local-minima image [#:threshold +float-max+]
@deffnx Procedure im-local-maxima image [#:threshold (- +float-max+)]
@deffnx Procedure im-local-minima-channel channel width height @
                                          [#:threshold +float-max+]
@deffnx Procedure im-local-maxima-channel channel width height @
                                          [#:threshold (- +float-max+)]

@indentedblock
All local mnima and maxima related procedures also accept the following
additional optional keyword arguments: [#:con 8] [#:marker 1.0]
[#:borders? #f] [#:plateaus?  #f] [#:epsilon 1.0e-4]
@end indentedblock

Returns a new image or channel.

Finds the local minima or maxima in @var{image} or @var{channel}. Local
minima or maxima are defined as @samp{points} that are not on the
borders (unless @code{#:borders?} is @code{#t}), and whose values are
lower or higher, respectively, then the values of all direct neighbors.
In the result image or channel, these points are marked using the
@var{#:marker} value (all other pixels values will be set to @code{0}).

By default, the algorithm uses 8-connectivity to define a neighborhood,
which can be changed passing the optional keyword argument @var{#:con},
which can be either 4 or 8.

The @var{#:threshold} optional keyword argument can be used to discard
minima and maxima whose (original pixel) value is not below or above the
threshold, respectively. Both default values depend on
@code{+float-max+}, which is defined (and so is @code{+float-min+})
using the corresponding limit value as given by the C float.h header.

The @var{#:plateaus?} optional keyword argument can be used to allow
regions of @samp{constant} (original pixel) value whose neighbors are
all higher (minima) or lower (maxima) than the value of the region. Tow
pixel values are considered part of the same region (representing the
same @samp{constant} value) if the absolute value of their difference is
@code{<=} to @var{#:epsilon}.

@strong{Notes}:
@itemize
@item If you want to know how many minima or maxima
were found, use @ref{im-reduce} upon the result;

@item If you are interested by the original minima or maxima pixel
values, Use @ref{im-times} between the original image and the result.
@end itemize
@end deffn


@anchor{im-fft}
@anchor{im-fft-channel}
@deffn Procedure im-fft image
@deffnx Procedure im-fft-channel channel width height
@cindex Fast Fourier Transform

Returns two images or channels.

@c A @emph{transform} is simply a mapping from one set of coordinates to
@c another. The Fourier tranform converts spatial coordinates into
@c frequencies.

Computes and returns the Fast Fourier Transform@footnote{The @abbr{FFT,
Fast Fourier Transform} is simply a faster way to compute the Fourier
transform. All FFT related procedures, and their inverse, are obtained
by calling the @uref{@value{UFFTW}, FFTW library}.} of the @var{image}
or @var{channel}. It returns two values, images or channels: the first
contains the real part and the second the imaginary part of the
transformation.
@end deffn


@anchor{im-fft-inverse}
@anchor{im-fft-inverse-channel}
@deffn Procedure im-fft-inverse real imaginary
@deffnx Procedure im-fft-inverse-channel real-channel @
                    imaginary-channel width height
@cindex Fast Fourier Transform Inverse

Returns two images or channels.

Computes and returns the inverse Fast Fourier Transform given its
@var{real} and @var{imaginary} or @var{real-channel} and
@var{imaginary-channel} parts. It returns two values, images or
channels: the first contains the real part and the second the imaginary
part of the inverse transformation.
@end deffn


@anchor{im-fcc}
@anchor{im-fcc-channel}
@anchor{im-fncc}
@anchor{im-fncc-channel}
@deffn Procedure im-fcc image mask
@deffnx Procedure im-fncc image mask
@deffnx Procedure im-fcc-channel i-chan m-chan @
                  width height m-width m-height
@deffnx Procedure im-fncc-channel i-chan m-chan @
                  width height m-width m-height
@cindex Fast Cross Correlation
@cindex Fast Normalized Cross Correlation

Returns an image or a channel.

Computes and returns the Fast Cross Correlation or Fast Normalized Cross
Correlation between @var{image} and a (usually smaller)
@var{mask}@footnote{Also called a @emph{template}, or a
@emph{pattern}.}, using the Fast Fourier Transform@footnote{Hence the
names, @abbr{FCC, Fast Cross Correlation} and @abbr{FNCC, Fast
Normalized Cross Correlation}.}.
@end deffn
